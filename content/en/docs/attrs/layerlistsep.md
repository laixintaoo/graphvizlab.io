---
defaults:
- '","'
flags: []
minimums: []
title: layerlistsep
types:
- string
used_by: G
description: Specifies the separator characters used to split an attribute of type
---
[`layerRange`](/docs/attr-types/layerRange/) into a list of ranges.
